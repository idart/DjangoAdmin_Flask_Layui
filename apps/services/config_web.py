# +----------------------------------------------------------------------
# | DjangoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
# +----------------------------------------------------------------------
# | 版权所有 2021~2023 北京DjangoAdmin研发中心
# +----------------------------------------------------------------------
# | Licensed LGPL-3.0 DjangoAdmin并不是自由软件，未经许可禁止去掉相关版权
# +----------------------------------------------------------------------
# | 官方网站: https://www.djangoadmin.cn
# +----------------------------------------------------------------------
# | 作者: @一米阳光 团队荣誉出品
# +----------------------------------------------------------------------
# | 版权和免责声明:
# | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
# | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
# | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
# | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
# | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
# | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
# | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
# | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
# | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
# +----------------------------------------------------------------------

import json
import logging

from flask import request
from sqlalchemy import and_

from apps.models.config import Config
from apps.models.config_data import ConfigData
from config.env import FLASK_IMAGE_URL
from utils import R
from utils.utils import getImageURL, saveImage


# 获取配置信息
def getConfigInfo():
    # 配置ID
    config_id = request.args.get('config_id', 1)
    # 查询配置数据列表
    data_list = ConfigData.query.filter(
        and_(ConfigData.config_id == config_id, ConfigData.status == 1, ConfigData.is_delete == 0)).order_by(
        ConfigData.sort.asc()).all()

    # 实例化配置数据对象
    dataList = []
    if data_list:
        for v in data_list:
            # 数据类型
            type = v.type

            data = {}
            data["id"] = v.id
            data["title"] = v.title
            data["code"] = v.code
            data["value"] = v.value
            data["type"] = v.type

            if type == "checkbox":
                # 复选框
                itemList = {}
                options = v.options.split(',')
                if len(options) > 0:
                    for val in options:
                        item = val.split('=')
                        itemList[item[0]] = item[1]
                data['itemList'] = itemList
            elif type == "radio":
                # 单选按钮
                itemList = {}
                options = v.options.split(',')
                if len(options) > 0:
                    for val in options:
                        item = val.split('=')
                        itemList[item[0]] = item[1]
                data['itemList'] = itemList
            elif type == "select":
                # 下拉选择
                itemList = {}
                options = v.options.split(',')
                if len(options) > 0:
                    for val in options:
                        item = val.split('=')
                        itemList[item[0]] = item[1]
                data['itemList'] = itemList
            elif type == "image":
                # 单图
                if v.value:
                    data["value"] = getImageURL(v.value)
            elif type == "images":
                # 多图
                if v.value:
                    # 字符串分裂处理
                    list = v.value.split(',')
                    itemList = []
                    for v in list:
                        image = getImageURL(v)
                        itemList.append(image)
                    # 图片数据
                    data["value"] = itemList
            # 加入数组
            dataList.append(data)

    # 获取配置列表
    configList = Config.query.filter(Config.is_delete == 0).all()

    # 模板参数
    content = {
        'config_id': int(config_id),
        'configList': configList,
        'dataList': dataList,
    }
    # 返回结果
    return content


# 保存配置信息
def saveConfigInfo():
    # 获取请求参数
    dict_data = request.json
    if not dict_data:
        return R.failed("请求参数异常")
    # 数据源处理
    for key in dict_data:
        # 参数值
        val = dict_data[key]
        if key.find('checkbox') != -1:
            # 复选框
            item = key.split('__')
            key = item[0]
        elif key.find('image') != -1:
            # 单图上传
            item = key.split('__')
            key = item[0]
            if val.find('temp') != -1:
                image = saveImage(val, "config")
                # 赋值
                val = image
            else:
                # 赋值
                val = val.replace(FLASK_IMAGE_URL, "")
        elif key.find('imgs') != -1:
            # 多图上传
            item = key.split('__')
            key = item[0]
            # 图片地址处理
            urlList = val.split(',')
            list = []
            if len(urlList) > 0:
                for url in urlList:
                    if url.find('temp') != -1:
                        url = saveImage(url, "config")
                        # 加入数组
                        list.append(url)
                    else:
                        # 赋值
                        url = url.replace(FLASK_IMAGE_URL, "")
                        list.append(url)
            # 图片地址逗号分割
            val = ','.join(list)
        elif key.find('ueditor') != -1:
            # 富文本
            item = key.split('__')
            key = item[0]

        # 根据编码查询配置项
        config_data = ConfigData.query.filter(and_(ConfigData.code == key, ConfigData.is_delete == 0)).first()
        if not config_data:
            continue
        # 设置
        config_data.value = val
        config_data.save()

    # 返回结果
    return R.ok()
