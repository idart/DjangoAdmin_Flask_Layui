# +----------------------------------------------------------------------
# | DjangoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
# +----------------------------------------------------------------------
# | 版权所有 2021~2023 北京DjangoAdmin研发中心
# +----------------------------------------------------------------------
# | Licensed LGPL-3.0 DjangoAdmin并不是自由软件，未经许可禁止去掉相关版权
# +----------------------------------------------------------------------
# | 官方网站: https://www.djangoadmin.cn
# +----------------------------------------------------------------------
# | 作者: @一米阳光 团队荣誉出品
# +----------------------------------------------------------------------
# | 版权和免责声明:
# | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
# | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
# | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
# | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
# | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
# | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
# | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
# | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
# | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
# +----------------------------------------------------------------------

import os
import re
import time

from flask import session

from config.env import FLASK_IMAGE_URL, FLASK_IMAGE_PATH, FLASK_TEMP_PATH, FLASK_UPLOAD_DIR
from utils.file import mkdir


# 获取用户ID
def uid():
    # 用户ID
    userId = session.get("user_id")
    if not userId:
        return 0
    # 返回结果
    return int(userId)


# 判断变量类型的函数
def typeof(value):
    type = None
    if isinstance(value, int):
        type = "int"
    elif isinstance(value, str):
        type = "str"
    elif isinstance(value, float):
        type = "float"
    elif isinstance(value, list):
        type = "list"
    elif isinstance(value, tuple):
        type = "tuple"
    elif isinstance(value, dict):
        type = "dict"
    elif isinstance(value, set):
        type = "set"
    return type


# 返回变量类型
def getType(value):
    arr = {"int": "整数", "float": "浮点", "str": "字符串", "list": "列表", "tuple": "元组", "dict": "字典", "set": "集合"}
    vartype = typeof(value)
    if not (vartype in arr):
        return "未知类型"
    return arr[vartype]


# 获取图片地址
def getImageURL(path):
    return FLASK_IMAGE_URL + path


def saveImage(url, dirname):
    # 判断文件地址是否为空
    if not url:
        return "文件地址不能为空"
    # 判断是否是本站图片
    if url.find(FLASK_IMAGE_URL) != -1:
        # 本站图片
        if url.find("temp") != -1:
            # 临时图片
            path = FLASK_IMAGE_PATH + "/" + dirname + "/" + time.strftime('%Y%m%d')
            # 创建存储目录
            mkdir(path)
            # 原始图片地址
            oldPath = url.replace(FLASK_IMAGE_URL, FLASK_UPLOAD_DIR)
            # 目标目录地址
            newPath = FLASK_IMAGE_PATH + "/" + dirname + oldPath.replace(FLASK_TEMP_PATH, "")
            # 移动文件
            os.rename(oldPath, newPath)
            # 返回结果
            return newPath.replace(FLASK_UPLOAD_DIR, "")
        else:
            # 非临时图片
            return url.replace(FLASK_IMAGE_URL, "")
    else:
        # 远程图片
        print("远程图片处理")
    # 返回空字符串
    return ""


# 保存富文本内容
def saveEditContent(content, title, dirname):
    # 正则取出富文本图片地址
    urlList = re.findall('img src="(.*?)"', content, re.S)
    # 遍历图片地址
    for url in urlList:
        # 保存图片至本地
        image = saveImage(url, dirname)
        if image:
            content = content.replace(url, "[IMG_URL]" + image)

    # 设置alt标题
    if content.find("alt=\"\"") and title != "":
        content = content.replace("alt=\"\"", "alt=\"" + title + "\"")
    # 返回结果
    return content
