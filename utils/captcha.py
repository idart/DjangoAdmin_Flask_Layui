# +----------------------------------------------------------------------
# | DjangoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
# +----------------------------------------------------------------------
# | 版权所有 2021~2023 北京DjangoAdmin研发中心
# +----------------------------------------------------------------------
# | Licensed LGPL-3.0 DjangoAdmin并不是自由软件，未经许可禁止去掉相关版权
# +----------------------------------------------------------------------
# | 官方网站: https://www.djangoadmin.cn
# +----------------------------------------------------------------------
# | 作者: @一米阳光 团队荣誉出品
# +----------------------------------------------------------------------
# | 版权和免责声明:
# | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
# | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
# | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
# | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
# | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
# | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
# | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
# | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
# | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
# +----------------------------------------------------------------------

import uuid
from io import BytesIO

from PIL import Image, ImageFont, ImageDraw, ImageFilter
import random
import base64

from flask import session


def get_random_color():
    # 获取随机颜色的数字rgb
    return random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)


def generate_image(length):
    """制作验证码图片

    :param length: 验证码的长度
    :return: image:验证码图片, code：验证码内容
    """

    # 设置验证码内容抽取的数据源
    code_ku = 'yti1z2332453hsidhgisdlcFGH7qQ941RTYUasdgsiotyDSFGureg1634641AGSDF3as1fa'
    # 创建画布
    image = Image.new('RGB', (200, 60), color='white')

    # 创建字体
    font = ImageFont.truetype("utils/fonts/Vera.ttf", size=30)

    # 创建ImageDraw对象
    draw = ImageDraw.Draw(image)

    # 绘制验证码
    code = ''
    for i in range(length):
        c = random.choice(code_ku)
        code += c
        draw.text((5 + random.randint(4, 7) + 25 * i, random.randint(4, 7)),  # (x,y坐标位置)，
                  # 左边距最小9，最大12，y轴最小4，最大7
                  text=c,
                  fill=get_random_color(),
                  font=font)

    # 绘制干扰线
    for i in range(10):
        x1 = random.randint(0, 200)
        y1 = random.randint(0, int(60 / 2))

        x2 = random.randint(0, 200)
        y2 = random.randint(int(60 / 2), 60)

        draw.line(((x1, y1), (x2, y2)), fill=get_random_color())

    image = image.filter(ImageFilter.EDGE_ENHANCE)  # 滤镜
    # image.show()  # 显示图片

    return image, code


# 生成验证码实例的函数
def generate_captcha():
    # 生成6位数验证码
    image, code = generate_image(6)
    # 将image对象转为二进制
    buffer = BytesIO()
    # 以这个格式保存
    image.save(buffer, 'png')
    # 读取buffer对象的内容
    buf_bytes = buffer.getvalue()
    # 将验证码转换为base64格式
    data = str(base64.b64encode(buf_bytes))[1:].strip("'")
    # 生成随机字符串
    idKey = str(uuid.uuid4())
    # 将验证码文本存入session，做用户登录认证时可用
    session[idKey] = code
    # 返回结果
    result = {
        "idKey": idKey,
        "data": data
    }
    return result
